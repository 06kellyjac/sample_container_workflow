#!/bin/sh

BASEDIR=$(dirname "$0")

LOCATION="$(cd ${BASEDIR}/../; echo $PWD)/docker-compose"

URL="https://github.com/docker/compose/releases/download/1.22.0/run.sh"

if command -v curl > /dev/null 2>&1; then
	echo "Using curl"
	curl -L --fail $URL -o $LOCATION
elif command -v wget > /dev/null 2>&1; then
	echo "Using wget"
	wget -O $LOCATION $URL
else
	echo "ERROR: Niether curl nor wget is installed on your machine :("
	exit 1
fi

chmod +x $LOCATION

echo "Downloaded to: $LOCATION and ready to use"
