#!/usr/bin/env node
const fs = require("fs");

const fileName = process.env.FILE;

const app = require(`../app_${fileName}.json`);
const server = require(`../server_${fileName}.json`);

const output = JSON.stringify(app.concat(server));

fs.writeFileSync(`${fileName}.json`, output);
