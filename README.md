# Sample Container Workflow

[pipeline_master]: https://gitlab.com/06kellyjac/sample_container_workflow/badges/master/pipeline.svg
[pipeline_develop]: https://gitlab.com/06kellyjac/sample_container_workflow/badges/develop/pipeline.svg
[pipeline_link]: https://gitlab.com/06kellyjac/sample_container_workflow/pipelines

[coverage_master]: https://gitlab.com/06kellyjac/sample_container_workflow/badges/master/coverage.svg
[coverage_master_app]: https://gitlab.com/06kellyjac/sample_container_workflow/badges/master/coverage.svg?job=test:app
[coverage_master_server]: https://gitlab.com/06kellyjac/sample_container_workflow/badges/master/coverage.svg?job=test:server
[coverage_develop]: https://gitlab.com/06kellyjac/sample_container_workflow/badges/develop/coverage.svg

<!-- markdownlint-disable MD013 -->

| Branch  |                          CI                           |                        App                        |                        Server                        |                Total Coverage                  |
| :-----: | :---------------------------------------------------: | :-----------------------------------------------: | :--------------------------------------------------: | :--------------------------------------------: |
| Master  | [![pipeline status][pipeline_master]][pipeline_link]  | [![coverage][coverage_master_app]][pipeline_link] | [![coverage][coverage_master_server]][pipeline_link] | [![coverage][coverage_master]][pipeline_link]  |

<!-- markdownlint-enable MD013 -->

## How to run the project

### Preparation

This project is designed to only require `docker` to do development.

Having `docker-compose` installed natively can be simpler but `docker-compose` can also run in-side of docker.

[compose_install]: https://docs.docker.com/compose/install/#install-compose

To install `docker-compose` [click here][compose_install].
If you want it **natively**, choose the tab for your operating system.
To run it **in-side of `docker`**, either run `.scripts/get_compose.sh` on your machine, or follow the manual
instructions in the "Alternative Install Options" tab under the heading "Install as a container".

### Running

It's advised that you have a precursor knowledge of `docker` and `docker-compose` but not required.

Before running any of the below commands it is strongly recommended to read the **Cleaning Up** section.

#### Development

The development `docker-compose` config is `docker-compose.yaml`.

It will mount the `app` and `server` folders inside the containers, install `npm` dependencies and run the `npm start` commands.

This will give you a development environment that will auto-reload on save.

```shell
docker-compose up
```

#### Building

The `compose/build.yaml` file is an extension to `docker-compose.yaml` and will build the projects.
To do this we add the flag `-f docker-compose.yaml` to use the base development `yaml` file and
`-f compose/build.yaml` to extend it.

```shell
docker-compose -f docker-compose.yaml -f compose/build.yaml up
```

#### Running production containers

The `compose/prod.yaml` file doesn't extend the development `yaml` file although it does require you to run the
command mentioned previously to build the project files.
It will build the docker images and run them.

The `--build` flag ensures that the images are rebuilt.
This is useful if have re-built the project files but if you haven't it can *minimally* slow down the startup time.

```shell
docker-compose -f compose/prod.yaml up --build
```

#### Running the containers in the Registry that were built by CI on GitLab

The `compose/registry.yaml` file will pull the images down from the Registry on GitLab.

This is very similar to the above config but it will use the actual images in the GitLab Registry that will eventually
be deployed.

They should be identical, this is more so that you can run the stack without building the project files and images,
and test the registry is working.

```shell
docker-compose -f compose/registry.yaml up
```

### Cleaning up

If you want to remove the containers and networks created by `docker-compose` you can run the command `docker-compose down`.

If you have used a flag such as `-f somefilename.yaml` then you need to include that.

e.g. `docker-compose -f somefilename.yaml down`.

If you haven't done this and have a bunch of containers and networks you can remove them individually with `docker` or,
if you have nothing `docker` related that you need, running `docker system prune -af` will clean up everything for you.

**NOTE:** the command `docker system prune -af` will remove `docker` images you've built locally, `docker` volumes that
may contain data, etc. It will leave running containers and their relevant res
