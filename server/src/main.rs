#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

use rocket_contrib::{Json, Value};

#[get("/")]
fn index() -> Json<Value> {
	Json(json!({
		"status": "ok",
		"message": "Hello World!"
	}))
}

#[get("/kube")]
fn kube() -> Json<Value> {
	Json(json!({
		"status": "ok",
		"message": "a b c"
	}))
}

#[catch(404)]
fn not_found() -> Json<Value> {
	Json(json!({
		"status": "error",
		"reason": "Resource was not found."
	}))
}

fn rocket() -> rocket::Rocket {
	rocket::ignite()
		.mount("/", routes![index, kube])
		.catch(catchers![not_found])
}

fn main() {
	rocket().launch();
}

#[cfg(test)]
mod test {
	use super::rocket;
	use rocket::http::Status;
	use rocket::local::Client;

	#[test]
	fn index() {
		let client = Client::new(rocket()).expect("valid rocket instance");
		let response = client.get("/").dispatch();
		assert_eq!(response.status(), Status::Ok);
	}
}
